package controllers

import (
	"back_end/middleware"
	"back_end/models"
	"back_end/services"
	"log"

	"github.com/gofiber/fiber/v2"
)

func GetUsers(c *fiber.Ctx) error {
	user, err := services.GetUsers()
	if err != nil {
		log.Fatal(err)
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Something went wrong",
			"Erro":    err.Error(),
		})
	}
	return c.Status(fiber.StatusOK).JSON(user)
}

func CreateUser(c *fiber.Ctx) error {
	var user models.User
	err := c.BodyParser(&user)

	if err != nil {
		c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse JSON",
			"error":   err.Error(),
		})
	}

	errors := middleware.ValidateStruct(user)
	if errors != nil {
		return c.JSON(errors)
	}

	_, filt, ID, _ := middleware.IdentityExists(user.NumeroIdentidad)
	if filt == true {
		return c.JSON(ID)
	}

	user, _ = services.CreaterUser(user)

	return c.Status(fiber.StatusOK).JSON(user)

}

func GetUser(c *fiber.Ctx) error {
	id := c.Params("id")
	user, err := services.GetUser(id)
	if err != nil {
		log.Fatal(err)
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse Id",
			"Erro":    err.Error(),
		})
	}
	return c.Status(fiber.StatusOK).JSON(user)
}

func UpdateUser(c *fiber.Ctx) error {
	id := c.Params("id")
	var user models.User
	err := c.BodyParser(&user)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse JSON",
			"error":   err,
		})
	}
	user, _ = services.UpdateUser(id, user)
	return c.Status(fiber.StatusOK).JSON(user)

}

func DeteledUser(c *fiber.Ctx) error {
	id := c.Params("id")
	user, err := services.DeletedUser(id)
	if err != nil {
		log.Fatal(err)
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse Id",
			"Erro":    err.Error(),
		})
	}
	return c.Status(fiber.StatusOK).JSON(user)
}
