package main

import (
	"back_end/db"
	"back_end/router"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func main() {
	/*if os.Getenv("APP_ENV") != "production" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}*/
	app := fiber.New()

	app.Use(logger.New())
	app.Use(cors.New())
	app.Use(compress.New())

	router.UserRouter(app)

	// config db
	db.ConnectDB()

	/*port := os.Getenv("PORT")
	err := app.Listen(":" + port)

	if err != nil {
		log.Fatal("Error app failed to start")
		panic(err)
	}*/

	log.Fatal(app.Listen(":3000"))
}
