package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	Id              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	PrimerApellido  string             `json:"primerapellido,omitempty" validate:"required,min=3,max=20"`
	SegundoApellido string             `json:"segundoapellido,omitempty" validate:"required,min=3,max=20"`
	PrimerNombre    string             `json:"primernombre,omitempty" validate:"required,min=3,max=30"`
	OtroNombre      string             `json:"otronombre,omitempty" validate:"min=3,max=50"`
	Pais            string             `json:"pais,omitempty" validate:"required"`
	TipoIdentidad   string             `json:"tipoidentidad,omitempty" validate:"required"`
	Area            string             `json:"area,omitempty" validate:"required"`
	NumeroIdentidad string             `json:"numeroindentidad,omitempty" validate:"required,min=3,max=20"`
	Email           string             `json:"email,omitempty" validate:"required,min=3,max=300"`
	CreatedAt       time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time          `json:"updated_at" bson:"updated_at"`
}
