package services

import (
	"back_end/db"
	"back_end/models"
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetUsers() ([]models.User, error) {
	var users []models.User

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	cursor, err := db.DB.Collection("user").Find(ctx, bson.D{})
	if err = cursor.All(ctx, &users); err != nil {
		log.Fatal(err)
		return users, err
	}
	return users, nil
}

func CreaterUser(user models.User) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	res, err := db.DB.Collection("user").InsertOne(ctx, user)
	if err != nil {
		log.Fatal(err)
		return models.User{}, err
	}
	fmt.Println(res.InsertedID)
	return user, nil
}

func GetUser(userId string) (models.User, error) {
	var user models.User
	objectId, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		log.Fatal(err)
		return user, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	err = db.DB.Collection("user").FindOne(ctx, bson.M{"_id": objectId}).Decode(&user)
	if err != nil {
		log.Fatal(err)
		return user, err
	}
	return user, nil
}

func UpdateUser(userId string, user models.User) (models.User, error) {
	objectId, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		log.Fatal(err)
		return user, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	filter := bson.M{"_id": objectId}

	update := bson.M{"$set": user}
	res, err := db.DB.Collection("user").UpdateOne(ctx, filter, update)
	if err != nil {
		log.Fatal(err)
		return models.User{}, err
	}
	fmt.Println(res.UpsertedID)
	return user, nil
}

func DeletedUser(userId string) (models.User, error) {
	var user models.User
	objectId, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		log.Fatal(err)
		return user, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	res, err := db.DB.Collection("user").DeleteOne(ctx, bson.M{"_id": objectId})
	if err != nil {
		log.Fatal(err)
		return user, err
	}
	fmt.Println(res.DeletedCount)
	return user, nil
}
