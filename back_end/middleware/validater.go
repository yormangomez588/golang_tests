package middleware

import (
	"back_end/db"
	"back_end/models"
	"context"
	"time"

	"github.com/go-playground/validator"
	"go.mongodb.org/mongo-driver/bson"
)

func ValidateStruct(user models.User) []*models.ErrorResponse {
	var errors []*models.ErrorResponse
	validate := validator.New()
	err := validate.Struct(user)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element models.ErrorResponse
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}

func IdentityExists(numeroindentidad string) (models.User, bool, string, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var user models.User

	err := db.DB.Collection("user").FindOne(ctx, bson.M{"numeroindentidad": numeroindentidad}).Decode(&user)
	ID := user.Id.Hex()
	if err != nil {
		return user, false, ID, err
	}
	return user, true, ID, err

}
