package router

import (
	"back_end/controllers"

	"github.com/gofiber/fiber/v2"
)

func UserRouter(app *fiber.App) {
	app.Get("/api/user", controllers.GetUsers)
	app.Post("/api/user", controllers.CreateUser)
	app.Get("/api/user/:id", controllers.GetUser)
	app.Patch("/api/user/:id", controllers.UpdateUser)
	app.Delete("/api/user/:id", controllers.DeteledUser)
}
